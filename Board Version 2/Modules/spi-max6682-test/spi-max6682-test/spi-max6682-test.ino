// the sensor communicates using SPI, so include the library:
#include <SPI.h>

// the other you need are controlled by the SPI library):
const int chipSelectPin = 3;

void setup() {
  Serial.begin(9600);

  // start the SPI library:
  SPI.begin();
  SPI.beginTransaction(SPISettings(4000000, MSBFIRST, SPI_MODE0));
  delay(100);
  SPI.endTransaction();

  // initalize the  data ready and chip select pins:
  pinMode(chipSelectPin, OUTPUT);

  // take the chip select low to select the device:
  digitalWrite(chipSelectPin, HIGH);
  
  delay(100);
}

uint8_t temp1=0, temp2=0;
void loop() {
     // take the chip select low to select the device:
    digitalWrite(chipSelectPin, LOW);
    // send the device the register you want to read:
    temp1 = SPI.transfer(0x00);
    // send a value of 0 to read the first byte returned:
    temp2 = SPI.transfer(0x00);

    Serial.print(temp1,BIN);
    Serial.println(temp2,BIN);

    temp2 = (temp1 << 2) | (temp2 & 0x03);
    temp2 = float(temp2) * 0.125;

    // take the chip select High to give chance to other devices:
    digitalWrite(chipSelectPin, HIGH);

    Serial.print("Temp[C]=");
    //Serial.print(temp1);
    Serial.println(temp2);

    delay(1000);
}
