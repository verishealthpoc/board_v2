/*************************************************

Register Address        Register Name     Initialised Value
0x01                        Rpmax             0x0E            83.111 k ohm
0x02                        Rpmin             0x3B             2.394 k ohm
0x03                        Sensor freq       -
0x04                        LDC Config         -
0x05                        CLK Config        -  
0x06                        Thres Hi LSB      0x50
0x07                        Thres Hi MSB      0x14
0x08                        Thres Lo LSB      0xC0
0x09                        Thres Lo MSB      0x12
0x0A                        INTB Config       0x04
0x0B                        PWR MODE          0x01






************************************************/





#include "SPI.h" // include arduino SPI library

const int CSB = 7; // chip select bit for Arduino Uno


void setup() 
{
  unsigned int data = 0;
  Serial.begin(9600);
  // start SPI library/ activate BUS
  SPI.begin();
 
  pinMode(CSB, OUTPUT); 
  SPI.setBitOrder(MSBFIRST);
  SPI.setDataMode(SPI_MODE0); // CPOL = 0 and CPH = 0 mode 3 also works
  SPI.setClockDivider(SPI_CLOCK_DIV4); // set SCLK @ 4MHz, LDC1000 max is 4MHz DIV2 also works
  
    // set power mode to idle to configure stuff
  digitalWrite(CSB, LOW);
  SPI.transfer(0x0B);
  SPI.transfer(0x00);
  digitalWrite(CSB, HIGH);
  delay(100);

 // Set RpMax
  digitalWrite(CSB, LOW);
  SPI.transfer(0x01);
  SPI.transfer(0x0E);
  digitalWrite(CSB, HIGH);
  delay(100);
  
  // Set RpMin
  digitalWrite(CSB, LOW);
  SPI.transfer(0x02);
  SPI.transfer(0x3B);
  digitalWrite(CSB, HIGH);
  delay(100);
  
   // Set Sensor frequency
  digitalWrite(CSB, LOW);
  SPI.transfer(0x03);
  SPI.transfer(0x94);
  digitalWrite(CSB, HIGH);
  delay(100);
  
   // Set LDC configurationn
  digitalWrite(CSB, LOW);
  SPI.transfer(0x04);
  SPI.transfer(0x17);
  digitalWrite(CSB, HIGH);
  delay(100);
  
   // Set clock configuration
  digitalWrite(CSB, LOW);
  SPI.transfer(0x05);
  SPI.transfer(0x00);
  digitalWrite(CSB, HIGH);
  delay(100);
  
  
  // disable all interrupt modes
  digitalWrite(CSB, LOW);
  SPI.transfer(0x0A);
  SPI.transfer(0x00);
  digitalWrite(CSB, HIGH);
  
  // set thresh HiLSB value
  digitalWrite(CSB, LOW);
  SPI.transfer(0x06);
  SPI.transfer(0x50);
  digitalWrite(CSB, HIGH);
  delay(100);
  
  // set thresh HiMSB value
  digitalWrite(CSB, LOW);
  SPI.transfer(0x07);
  SPI.transfer(0x14);
  digitalWrite(CSB, HIGH);
  delay(100);
  
  // set thresh LoLSB value
  digitalWrite(CSB, LOW);
  SPI.transfer(0x08);
  SPI.transfer(0xC0);
  digitalWrite(CSB, HIGH);
  delay(100);
  
  // set thresh LoMSB value
  digitalWrite(CSB, LOW);
  SPI.transfer(0x09);
  SPI.transfer(0x12);
  digitalWrite(CSB, HIGH);
  delay(100);
  
  // set power mode to active mode
  digitalWrite(CSB, LOW);
  SPI.transfer(0x0B);
  SPI.transfer(0x01);
  digitalWrite(CSB, HIGH);
  delay(100);
  
  /*
// Read Rpmax
  digitalWrite(CSB, LOW);
  SPI.transfer(0x81); 
  data = SPI.transfer(0x00);
  Serial.println(data);
  digitalWrite(CSB, HIGH);
  delay(200);
// Read Rpmin
  digitalWrite(CSB, LOW);
  SPI.transfer(0x82); 
  data = SPI.transfer(0x00);
  Serial.println(data);
  digitalWrite(CSB, HIGH);
  delay(200); 
// Read thresh HiLSB value
  digitalWrite(CSB, LOW);
  SPI.transfer(0x86); 
  data = SPI.transfer(0x00);
  Serial.println(data);
  digitalWrite(CSB, HIGH);
  delay(200);
// Read thresh HiMSB value
  digitalWrite(CSB, LOW);
  SPI.transfer(0x87); 
  data = SPI.transfer(0x00);
  Serial.println(data);
  digitalWrite(CSB, HIGH);
  delay(200); 
// Read thresh LoLSB value
  digitalWrite(CSB, LOW);
  SPI.transfer(0x88); 
  data = SPI.transfer(0x00);
  Serial.println(data);
  digitalWrite(CSB, HIGH);
  delay(200); 
// Read thresh LoMSB value
  digitalWrite(CSB, LOW);
  SPI.transfer(0x89); 
  data = SPI.transfer(0x00);
  Serial.println(data);
  digitalWrite(CSB, HIGH);
  delay(200); 
  */ 
}


unsigned long read_frequency_data()
{

  unsigned int datafLSB = 0;
  unsigned int dataMidB = 0;
  unsigned int datafMSB = 0;
  unsigned long frequencyData = 0; //long to store 24bit long number
  byte zeros = 0b00000000;

  
  // Read frequency data LSB register
  digitalWrite(CSB, LOW);
  SPI.transfer(0xa3); 
  datafLSB = SPI.transfer(0x00);
  digitalWrite(CSB, HIGH);
  delay(100);
  
  // Read frequency data MidB register
  digitalWrite(CSB, LOW);
  SPI.transfer(0xa4); 
  dataMidB = SPI.transfer(0x00);
  digitalWrite(CSB, HIGH);
  delay(100);
  
  // Read frequency data MSB register
  digitalWrite(CSB, LOW);
  SPI.transfer(0xa5); 
  datafMSB = SPI.transfer(0x00);
  digitalWrite(CSB, HIGH);
  delay(100);


  frequencyData = (zeros << 8)|(datafMSB);
  frequencyData = (frequencyData << 8) | (dataMidB);
  frequencyData = (frequencyData << 8)| (datafLSB);

  return frequencyData;
}




unsigned int read_proximity_data()
{

  unsigned int dataLSB = 0;
  unsigned int dataMSB = 0;
  unsigned int proximityData = 0;

  
  // Read proximity data LSB register
  digitalWrite(CSB, LOW);

  SPI.transfer(0xA1);     
  dataLSB = SPI.transfer(0x00);
  //Serial.println(dataLSB);
  digitalWrite(CSB, HIGH);
  delay(100);
  
  // Read proximity data MSB register
  digitalWrite(CSB, LOW);
  SPI.transfer(0xA2);     
  dataMSB = SPI.transfer(0x00);
  //Serial.println(dataMSB);
  digitalWrite(CSB, HIGH);
  delay(100);
 
  proximityData = ((unsigned int)dataMSB << 8) | (dataLSB);

  return proximityData;
} 

void display_data(unsigned long disData,char * msg)
{
  Serial.print(msg);
  Serial.println(disData);
  
}

void loop() 
{
  unsigned int val = 0;
  unsigned int proximity_data=0;
  unsigned long frequency_data=0;


  proximity_data = read_proximity_data();   //get the proximity data 
  frequency_data = read_frequency_data();   //get the Frequency data 

  
  //   display_data function arguents :   1st argument : actual data  2nd data : Message
  display_data(proximity_data,"Proximity Data :");
  display_data(frequency_data,"Frequency data :");
  
  
}
