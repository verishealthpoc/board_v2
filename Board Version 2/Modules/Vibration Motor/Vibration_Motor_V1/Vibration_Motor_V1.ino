
#define VIBRATION_AMPLITUDE_CHANGE_BTN   8

#define MOTOR_PIN 5



/*
 * MOTOR_SELECTION = 1  = 310 - 003
 * MOTOR_SELECTION = 2  = C08-005
 * MOTOR_SELECTION = 3  = 310 - 103
 */


#define MOTOR_SELECTION 1



volatile int analog_value=0;

volatile short mode=0;

uint8_t NO_OF_MOTOR_AMPLITUDE = 0;

volatile boolean newButtonState =HIGH;
volatile boolean oldButtonState =HIGH;


void setup() {
  Serial.begin(9600);
  pinMode(VIBRATION_AMPLITUDE_CHANGE_BTN, INPUT_PULLUP);
  pinMode(MOTOR_PIN, OUTPUT);

  switch(MOTOR_SELECTION){
    case 1 :
      NO_OF_MOTOR_AMPLITUDE = 4;
      break;

    case 2 :
      NO_OF_MOTOR_AMPLITUDE = 4;
      break;

    case 3 :
      NO_OF_MOTOR_AMPLITUDE = 6;
      break;
      
  }
  
}

void loop() {
  
  // Get current button state.
  newButtonState = digitalRead(VIBRATION_AMPLITUDE_CHANGE_BTN);
  

  // Check if state changed from high to low (button press).
  if(  (newButtonState == LOW) && (oldButtonState == HIGH) ) {
    // Short delay to debounce button.
    delay(20);
    // Check if button is still low after debounce.
    newButtonState = digitalRead(VIBRATION_AMPLITUDE_CHANGE_BTN);
    
    if(newButtonState == LOW  ) { // Yes, still low

      mode++;
      mode = mode % NO_OF_MOTOR_AMPLITUDE;

      switch(mode) {
                  
        case 0:
          analog_value = 0;
          break;
          
        case 1:
          analog_value = 26 ;   //0.5 V
          break;
          
        case 2:
          analog_value = 51;   //1 V
          break;
          
        case 3:
          analog_value = 77;   //1.5 V
          break;
          
        case 4:
          analog_value = 102;  //2V
          break;

        case 5:
          analog_value = 128;  //2.5V
          break;

      }
      Serial.print(mode);

      
    }
  }

  // Set the last-read button state to the old state.
  oldButtonState = newButtonState;


  analogWrite(MOTOR_PIN, analog_value);
  delay(1000);
  analogWrite(MOTOR_PIN, 0);
  delay(1000);
  
}
