
#define VIBRATION_AMPLITUDE_CHANGE_BTN   8

#define MOTOR_PIN 5



/*
 * MOTOR_SELECTION = 1  = 310 - 003
 * MOTOR_SELECTION = 2  = C08-005
 * MOTOR_SELECTION = 3  = 310 - 103
 */


#define MOTOR_SELECTION 1



volatile int analog_value=0;

volatile short mode=0;

uint8_t NO_OF_MOTOR_AMPLITUDE = 0;



void setup() {
  Serial.begin(9600);
  pinMode(VIBRATION_AMPLITUDE_CHANGE_BTN, INPUT_PULLUP);
  pinMode(MOTOR_PIN, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(VIBRATION_AMPLITUDE_CHANGE_BTN), mode_change, FALLING);

  switch(MOTOR_SELECTION){
    case 1 :
      NO_OF_MOTOR_AMPLITUDE = 4;
      break;

    case 2 :
      NO_OF_MOTOR_AMPLITUDE = 4;
      break;

    case 3 :
      NO_OF_MOTOR_AMPLITUDE = 6;
      break;
      
  }
}

void loop() {

 
  analogWrite(MOTOR_PIN, analog_value);
  delay(1000);
  analogWrite(MOTOR_PIN, 0);
  delay(1000);
  
}


void mode_change(){
      mode++;
      mode = mode % NO_OF_MOTOR_AMPLITUDE;

      switch(mode) {
                  
        case 0:
          analog_value = 0;
          break;
          
        case 1:
          analog_value = 26 ;   //0.5 V
          break;
          
        case 2:
          analog_value = 51;   //1 V
          break;
          
        case 3:
          analog_value = 77;   //1.5 V
          break;
          
        case 4:
          analog_value = 102;  //2V
          break;

        case 5:
          analog_value = 128;  //2.5V
          break;

      }
      Serial.println(mode);
  
} 
