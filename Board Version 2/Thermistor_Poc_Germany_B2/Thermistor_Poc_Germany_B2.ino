// the sensor communicates using SPI, so include the library:
#include <SPI.h>

//#define MISO   12//MISO
//#define SCK    13//Clock
#define CS  3 //Selection Pin



#define THERMISTOR_VOL_DIV_PIN A7
#define LED1 5
#define LED2 4
#define NO_OF_FILTERS 10


#define VCC 5

#define RESISTANCE_IN_VOL_DIV 15000
#define LM35_PIN  A0

const int THERMISTOR_PULL_UP=0;


float val_lm35=0;

float cel=0;


float val_thr_vol_div=0;

float MAX6682_temp_value=0;


int LedState=LOW;



void setup() 
{
    Serial.begin(9600);
    while(!Serial);
    //set pin modes
    //pinMode(CS, OUTPUT);
    //pinMode(MISO, INPUT);
    //pinMode(SCK, OUTPUT);
   
    // start the SPI library:
    SPI.begin();
    SPI.beginTransaction(SPISettings(4000000, MSBFIRST, SPI_MODE0));
    delay(100);
    SPI.endTransaction();
  
    // initalize the  data ready and chip select pins:
    pinMode(CS, OUTPUT);


    pinMode(LED1, OUTPUT);
    pinMode(LED2, OUTPUT);
  
    // take the chip select low to select the device:
    digitalWrite(CS, HIGH);
    delay(100);

    Serial.println("MAX6682       LM35       Resistance Divider Circuit");
}





float MAX6682()
{
    // take the chip select low to select the device:
    
    uint16_t temp_final=0;
    float MAX6682_temperature=0;
    bool sign_bit=false;
    uint8_t temp1=0, temp2=0;

    digitalWrite(CS, LOW);

    temp1 = SPI.transfer(0x00);
    // send a value of 0 to read the first byte returned:
    temp2 = SPI.transfer(0x00);  //for fraction
    temp_final= temp1 & 0b01111111;
    MAX6682_temperature =(float) temp_final + (float)((temp2 & 0xE0)>>5)/10;
    
    // take the chip select High to give chance to other devices:
    digitalWrite(CS, HIGH);

    
    return MAX6682_temperature;
}








int adc_filter(int pin_number)
{

  unsigned int filter_value=0;
  
  for(int i=0;i<NO_OF_FILTERS;i++)
  {
    filter_value += analogRead(pin_number);// read the value from the analog channel
    delay(10);
  }
  
  filter_value/= NO_OF_FILTERS;
  return filter_value;
}









float read_lm_35()
{
  unsigned int lm35_digital_value=0;
  float mv=0;
  float celcius_value=0;
  
  lm35_digital_value=adc_filter(LM35_PIN);
  mv = ( lm35_digital_value/1024.0)*5000;
  celcius_value = mv/10+0.5;
  
  return celcius_value;
}








float read_thermistor_Vol_div()
{
  int val=0;
  float Vout=0;
  float thermistor_resistance=0;
  float temperature=0;
  
  val=adc_filter(THERMISTOR_VOL_DIV_PIN);  //analogRead(THERMISTOR_VOL_DIV_PIN);
  
  Vout= (float)val*5/1024;
  
  if(THERMISTOR_PULL_UP)
    thermistor_resistance = ((RESISTANCE_IN_VOL_DIV)*(VCC-Vout))/Vout;
  else 
    thermistor_resistance = Vout*RESISTANCE_IN_VOL_DIV/(VCC-Vout);
  
  

  //temperature = (resistance -34711)/(-558);
  //temperature = -27.8 + (9.92 * pow(10,-3)*thermistor_resistance) -3.63 * pow(10,-7) * pow(thermistor_resistance,2);

  temperature = 110 - 0.184 * val + 7.58 * pow(10,-5) * pow(val,2);   //derived from latest Temperature data 

  //Serial.print("val is ");
  //Serial.println(val);
  //Serial.print("Voltage is ");
  //Serial.println(Vout);
  //Serial.print("thermistor_resistance is ");
  //Serial.println(thermistor_resistance);
  //Serial.print("Temperature is ");
  //Serial.println(temperature);
  
  return temperature;
}







void blink_led()
{
  LedState = !LedState;
  digitalWrite(LED1, LedState);  
  digitalWrite(LED2, LedState);
}




void loop() {

  /////////////////////////////////////////////////////MAX6682 /////////////////////////////////////////////////////////////////////////////////////
  MAX6682_temp_value =  MAX6682();
  //Serial.print("MAX6682 Temperature Reading is = ");
  Serial.print(MAX6682_temp_value);
  Serial.print(" ,        ");
  /////////////////////////////////////////////////////MAX6682 /////////////////////////////////////////////////////////////////////////////////////
  


  /////////////////////////////////////////////////////LM35 /////////////////////////////////////////////////////////////////////////////////////
  val_lm35 = read_lm_35();
  //Serial.print("TEMPRATURE = ");
  Serial.print(val_lm35);
  Serial.print(" ,            ");
  /////////////////////////////////////////////////////LM35 /////////////////////////////////////////////////////////////////////////////////////

  
  /////////////////////////////////////////////////////Thermistor Voltage Divider ////////////////////////////////////////////////////////////////
  val_thr_vol_div = read_thermistor_Vol_div();
  //Serial.print("Temperature thermistor(Voltage Divider Circuit : ");
  Serial.print(val_thr_vol_div);
  /////////////////////////////////////////////////////Thermistor Voltage Divider ////////////////////////////////////////////////////////////////

  Serial.println();

  /////////////////////////////////////////////////////LED Blink////////////////////////////////////////////////////////////////////////////////
  blink_led();
  /////////////////////////////////////////////////////LED Blink////////////////////////////////////////////////////////////////////////////////

  
  delay(1000);
  
}
